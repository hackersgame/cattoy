#!/usr/bin/python3
# Python program to implement  
# WebCam Motion Detector 
  
# importing OpenCV, time and Pandas library 
import cv2, time, pandas 
# importing datetime class from datetime library 
from datetime import datetime 
import imutils
import np
import os
import random
from random import seed
from random import randint
from threading import Thread
import math
import sys

#sudo zypper in python3-pyserial
#sudo usermod -a -G dialout <cattoy_user>
import serial
import io

# Initializing DataFrame, one column is start  
# time and other column is end time 
#df = pandas.DataFrame(columns = ["Start", "End"]) 

#serial settings
debug_AI = False
serial_dev = "/dev/ttyUSB1"
ser = serial.Serial(serial_dev, 115200, timeout=0.01)
sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser))

# Capturing video
video = cv2.VideoCapture(0) 
check, last_frame = video.read() 

last_frame = cv2.cvtColor(last_frame, cv2.COLOR_BGR2GRAY)
last_gray = None

room_mapped = True
display_size = [500,650]
top_left = [75.87, 74.34]
bot_left = [26.37, 75.24]
top_right = [73.17, 17.64]
bot_right = [26.37, 16.74]
#[x_min/max], [y_min/max]
room = [[],[]]
#where the AI moves to
laser_home =[42,25]


def sizeof_fmt(num, suffix='B'):
    ''' by Fred Cirera,  https://stackoverflow.com/a/1094933/1870254, modified'''
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f %s%s" % (num, 'Yi', suffix)
    
    
def write_new_hw_min_max():
    global sio
    global top_left
    global bot_left
    global top_right
    global bot_right
    x_hw_cmd = f"x~{bot_right[0]}:{top_right[0]}\n"
    y_hw_cmd = f"y~{bot_right[1]}:{bot_left[1]}\n"
    #print(f"Moving: {x} {y}")
    sio.write(x_hw_cmd + y_hw_cmd)
    sio.flush()
    
def AI(laser_pos,movement_center,amount_of_movement):
    global laser_home
    #print()
    #print(f"Laser: {laser_pos}\nMovement at: {movement_center}")
    x = 0 #X data is kept in [0]
    y = 1 #Y data is kept in [1]
    AI_X_change = 0
    AI_Y_change = 0
    move_dist = dist(laser_pos[0], laser_pos[1], movement_center[0],movement_center[1])
    home_dist = dist(laser_pos[0], laser_pos[1], laser_home[0], laser_home[1])
    #print(f"\n\ndist to laser: {move_dist}")
    if debug_AI:
        print(f"\n\ndist to home: {home_dist}")
        print(f"laser: {laser_pos}")
        #print(f"move: {movement_center}")
        print(f"home: {laser_home}")
    #max move is the area of movement
    AI_jump = (amount_of_movement/600) * abs(move_dist)
    print("WTF: " + str(AI_jump))
    #if we are too close, jump away.
    if move_dist < 100 and AI_jump < 500:
        AI_jump = AI_jump + 1000
        print("Touched!")
    #if the laster is within it's "home", pick a new home
    if home_dist < 100 or move_dist < 100:
        print("picking new laser home")
        top_left_home = [42,25]
        top_right_home = [31, 422]
        bot_right_home = [590, 461]
        bot_left_home = [590,25]
        possible_next_points = [top_left_home,top_right_home,bot_right_home,bot_left_home]
        
        if laser_home == top_left_home:
            possible_next_points.remove(top_left_home)
            #laser_home = top_right_home
        elif laser_home == top_right_home:
            possible_next_points.remove(top_right_home)
            #laser_home = bot_right_home
        elif laser_home == bot_right_home:
            possible_next_points.remove(bot_right_home)
            #laser_home = bot_left_home
        elif laser_home == bot_left_home:
            possible_next_points.remove(bot_left_home)
            #laser_home = top_left_home
        laser_home = random.choice(possible_next_points)
        print(possible_next_points)
    #amount_of_movement is the total area of movement (at least 200)
    #AI_jump = amount_of_movement + randint(-50, 50)
    AI_jump = AI_jump/20
    total_move = abs(movement_center[0] - laser_pos[0]) + abs(movement_center[0] - laser_pos[0])
    if total_move == 0:
         total_move = 1
    amount_x = (100/total_move) * abs(movement_center[0] - laser_pos[0])
    amount_x = (AI_jump/100) * amount_x
    amount_y = (100/total_move) * abs(movement_center[1] - laser_pos[1])
    amount_y = (AI_jump/100) * amount_y
    
    
    if laser_home[x] > 100:
        #print("Right")
        if laser_home[y] > 100:
            #print("Bot")
            amount_y = amount_y * -1
            amount_x = amount_x * -1
        else:
            #print("Top")
            amount_y = amount_y * -1
    else:
        #print("Left")
        if laser_home[y] > 100:
            amount_x = amount_x * -1
            #print("Bot")
        else:
            pass
            #print("Top")
    #print((amount_x, amount_y))
    return((amount_x, amount_y))

def dist(x1,y1,x2,y2):  
     dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
     return dist  
 
def set_mouse_pos(x=960,y=540):
    os.system(f"xdotool mousemove {x} {y}")
    return([x,y])

def guess_laser_pos(x_angle, y_angle):
    global laser_pos
    total_units_down = top_left[0] - bot_left[0]
    units_down = display_size[0]/total_units_down
    units_down = display_size[0] + (units_down * (bot_left[0] - float(x_angle)))
    
    total_units_right = top_left[1] - top_right[1]
    units_right = display_size[1]/total_units_right
    units_right = display_size[1] + (units_right * (top_right[1] - float(y_angle)))
    #print(units_down)
    if units_down > 0 or units_right > 0:
        return([int(units_down), int(units_right)])
    else:
        #we don't know, return what we had before
        return(laser_pos)
    
    
def move_if_at_edge(pos):
    x = pos[0]
    y = pos[1]
    x_size = 1440
    y_size = 900
    edge_size = 20
    if x + edge_size > x_size:
        return(set_mouse_pos())
    if x - edge_size < 0:
        return(set_mouse_pos())
    if y + edge_size > y_size:
        return(set_mouse_pos())
    if y - edge_size < 0:
        return(set_mouse_pos())
    #print(pos)
    return(pos)

def get_mouse_pos(last_retry=False):
    try:
        #This leaks memory very bad
        #data = display.Display().screen().root.query_pointer()._data
        data = {}
        data['root_x'] = 500
        data['root_y'] = 500
    except Exception as e:
        if last_retry:
            print("Error getting mouse...\n\n\nPython Error:")
            raise(e)
        print("Running xhost +")
        os.system("xhost +")
        #return one time now that we run xhost +
        return(get_mouse_pos(last_retry=True))
    return((data["root_x"], data["root_y"]))

def move_laser(x,y):
    global sio
    #print(f"Moving: {x} {y}")
    sio.write(f"x:{x}\ny:{y}\n")
    sio.flush()
    data = sio.read(99)
    x_angle = None
    y_angle = None
    if len(data) > 0:
        data = data.strip().split("\n")
        #               4=number of replys from the LCM we expect
        if len(data) == 4:
            x_angle = data[-2].split(":")[-1].strip()
            y_angle = data[-1].split(":")[-1].strip()
            return ([x_angle,y_angle])
    return [0,0]
        
def update_video():
    global new_frame
    frame = None
    check = None
    while True:
        check, frame = video.read()
        if check:
            new_frame = frame
        
def update_mouse():
    global y_to_move
    global x_to_move
    global laser_pos
    global last_movement_center
    global amount_of_movement
    global last_angle
    last_angle = [0,0]
    amount_of_movement = 0
    new_frame = None
    x_to_move = 0
    y_to_move = 0
    last_mouse = get_mouse_pos()
    new_mouse = None
    x_change = None
    y_change = None
    AI_movement = None
    while True:
        new_mouse = get_mouse_pos()
        x_change = last_mouse[1] - new_mouse[1]
        y_change = last_mouse[0] - new_mouse[0]
        new_mouse = move_if_at_edge(new_mouse)
        
        if x_change != 0 or y_change != 0:
            #print(f"move {x_change}:{y_change}")
            x_to_move = x_to_move + x_change
            y_to_move = y_to_move + y_change
        
        laser_pos = guess_laser_pos(last_angle[0], last_angle[1])
        if room_mapped:
            AI_movement = AI([laser_pos[1],laser_pos[0]],last_movement_center,amount_of_movement)
        else:
            AI_movement = [0,0]
        #print("AI: " + str(AI_movement))
        x_to_move = x_to_move + AI_movement[0]
        y_to_move = y_to_move + AI_movement[1]
        
        if abs(x_to_move) > 0 or abs(y_to_move) > 0:
            last_angle = move_laser(int(x_to_move), int(y_to_move))
            x_to_move = 0
            y_to_move = 0
        
        last_mouse = new_mouse
print("Move laser to top left of image and hit space")
last_found_laser_pos = [0,0]
last_movement_center = [0,0]

def init():
    global last_frame
    global new_frame
    global laser_pos
    global last_movement_center
    global last_found_laser_pos
    global amount_of_movement
    global last_angle
    global x_to_move
    global y_to_move
    new_frame = None
    if room_mapped:
        write_new_hw_min_max()
    laser_pos = [0,0]
    last_mouse = get_mouse_pos()
    last_loop_time = time.time()
    video_thread = Thread(target=update_video, args=())
    video_thread.daemon = True
    video_thread.start()
    
    mouse_thread = Thread(target=update_mouse, args=())
    mouse_thread.daemon = True
    mouse_thread.start()
    time_taken = None
    tainted_frame = None
    frame = None
    x = None
    y = None
    w = None
    h = None
    frameDelta = None
    thresh = None
    big_cnts = None
    center_total_x = None
    center_total_y = None
    center_number = None
    area = None
    center_x = None
    center_y =None
    px_size = None
    key = None
    #Infinite while loop to treat stack of image as video 
    while True: 
        time_taken = time.time() - last_loop_time
        #print(time_taken * 1000)
        #update laser positon based on mouse movement

        #Reading frame(image) from video
        frame = new_frame 
        tainted_frame = cv2.cvtColor(frame, 0)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        
        #laser_pos = guess_laser_pos(last_angle[0], last_angle[1])
        if laser_pos:
            y = int(laser_pos[0])
            x = int(laser_pos[1])
            last_found_laser_pos = laser_pos
            cv2.rectangle(tainted_frame, (x, y), (x + 2, y + 2), (255, 0, 255), 2)
        else:
            x, y = last_found_laser_pos
            cv2.rectangle(tainted_frame, (x, y), (x + 2, y + 2), (255, 0, 255), 2)
        
        frameDelta = cv2.absdiff(last_frame, frame)
        thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
        big_cnts = cv2.findContours(thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        big_cnts = imutils.grab_contours(big_cnts)
        center_total_x = 0
        center_total_y = 0
        center_number = 0
        amount_of_movement = 0
        for c in big_cnts:
            (x, y, w, h) = cv2.boundingRect(c)
            area = cv2.contourArea(c)
            if area > 2:
                center_x = int(x+(w/2))
                center_y = int(y+(h/2))
                center_total_x = center_total_x + center_x
                center_total_y = center_total_y + center_y
                center_number = center_number + 1
                amount_of_movement = amount_of_movement + area
        if center_number > 0:
            px_size = int(amount_of_movement/100)
            x = int(center_total_x/center_number) - int(px_size/2)
            y = int(center_total_y/center_number) - int(px_size/2)
            last_movement_center = [x,y]
            cv2.rectangle(tainted_frame, (x, y), (x + px_size, y + px_size), (0, 255, 0), 2)
        else:
            x, y = last_movement_center
            px_size = 2
            cv2.rectangle(tainted_frame, (x, y), (x + px_size, y + px_size), (0, 255, 0), 2)

        if not laser_pos:
            laser_pos = last_found_laser_pos
        if center_number == 0:
            x, y = last_movement_center
            
        cv2.imshow('frame', tainted_frame)
        last_loop_time = time.time()
        #cv2.imshow('frameDelta', frameDelta)
        #cv2.imshow('color', frame)        
        
        #cnts = imutils.grab_contours(cnts)
        #if cv2.waitKey(33) >= 0:
            #break

        last_frame = frame
        #last_gray = gray
        
        
        key = cv2.waitKey(1) 
        # if q entered whole process will stop
        if key != -1:
            print(key)
        if not room_mapped:
            if key == 32:
                print(f"[{last_angle[0]}, {last_angle[1]}]")
                #print(last_found_laser_pos)

        
        #Key Left
        if key == 83:
            #x_to_move = x_to_move + 100
            y_to_move = y_to_move - 10
        #Key Right
        if key == 81:
            y_to_move = y_to_move + 10
        #Key Down
        if key == 84:
            x_to_move = x_to_move - 10
        #Key Up
        if key == 82:
            x_to_move = x_to_move + 10
        if key == ord('q'):
            print("exit")
            break
        


init() #exits on keyboard input 'q'

video.release() 
  
# Destroying all the windows 
cv2.destroyAllWindows() 
